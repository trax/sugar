#include "Http.hpp"
#include <stdexcept>
#include <functional>

namespace sugar {
using namespace std::placeholders;

Return<std::string> Http::init (){
 
    if (curl_global_init(CURL_GLOBAL_ALL)){
	return {false, "Could not init libcurl"};
    }
 
    curl = curl_easy_init();
    if (!curl) {
	return {false, "Could not get handle from libcurl"};
    }
    
    return true;
}

Http::Http(){
    if (!init()){
	throw std::runtime_error("Could not init http class");
    }
}

Http::Http(const std::string &login, 
	   const std::string &pass) {
    
    if (!init()){
	throw std::runtime_error("Could not init http class");
    }
    

    curl_easy_setopt(curl, CURLOPT_USERNAME, login.c_str());
    curl_easy_setopt(curl, CURLOPT_PASSWORD, pass.c_str());
    
}


size_t Http::getData (void *contents, size_t size, size_t nmemb, void *userp){
    static_cast<std::string*>(userp)->append(static_cast<char*>(contents), size*nmemb);
    
    return size*nmemb;
}

const std::string Http::get (const std::string &url) {
    CURLcode res;
    buf.clear();
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &Http::getData);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buf);
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    
    res = curl_easy_perform(curl);
    if(res != CURLE_OK) {
	std::string msg = "Could not perform request";
	msg.append(curl_easy_strerror(res));
	return "";
    }

    return buf;
}

Http::~Http(){
    curl_easy_cleanup(curl);
    curl_global_cleanup();
}

}



#ifdef SUGAR_TEST
#include <iostream>

int main (int argc, char *argv[]){
    {sugar::Http http;
    if (argc == 1){
	std::cout << http.get ("http://localhost:8080");
    }else {
	std::cout << http.get (argv[1]);
    }
    }

    {sugar::Http http("admin", "changedyou2");
    if (argc == 1){
	std::cout << http.get ("http://localhost:9001");
    }else {
	std::cout << http.get (argv[1]);
    }
    }
    
    return 0;
}
 #endif 
