#ifndef SUGAR_RETURN_HPP_
#define SUGAR_RETURN_HPP_


template <class T>
class Return {

public:
  T data;
  bool success;

  Return (bool _success):
    success(_success) {
  }
  
  Return (bool _success, T _data): 
    data(_data),
    success(_success) {
  }

  Return (T _data): 
    data(_data),
    success(true) {
  }

  template<typename CurrentType>
  Return (const Return <CurrentType> &r){
    data     = r.data;
    success  = r.success;
  }

  template<typename CurrentType>
  Return (Return <CurrentType> &r){
    data     = r.data;
    success  = r.success;
  }


  operator bool () const {
    return success;
  }

  operator T () {
    return data;
  }

};

template<>
class Return < void > {

public:
  bool success;

  Return (bool _success):
    success (_success){
  }
};

#endif


