#ifndef DOLTIX_SUGAR_HTTP_HPP
#define DOLTIX_SUGAR_HTTP_HPP

#include <string>
#include <curl/curl.h>
#include "Return.hpp"

namespace sugar {

class Http {
private:
    CURL *curl;
    std::string buf; 
    static size_t getData (void *contents, size_t size, size_t nmemb, void *userp);
    
public:
    Http();
    Http(const std::string &login, 
	 const std::string &pass);
    Return<std::string> init();
    
    const std::string get (const std::string &url);

    ~Http();
};
}


#endif /* DOLTIX_SUGAR_HTTP_HPP */
